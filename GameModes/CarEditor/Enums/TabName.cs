﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor.Enums
{
    /// <summary>
    /// Tab names.
    /// </summary>
    enum TabName
    {
        None,
        HullEditor,
        CarParameters,
        ParticleParameters,
        BodyParameters,
        WheelParameters,
        AerialParameters,
        SpringParameters,
        AxleParameters,
        ModelEditor,
        EditorMenu
    }
}
