﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor.Enums
{
    /// <summary>
    /// Car editor input actions.
    /// </summary>
    enum EditorAction
    {
        Rotate,
        Zoom,
        SlideX,
        SlideY,
        Move
    }
}
