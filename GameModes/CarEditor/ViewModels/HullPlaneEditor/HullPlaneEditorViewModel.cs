﻿using OpenVolt.SDK.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor.ViewModels.HullPlaneEditor
{
    /// <summary>
    /// Hull editor view model.
    /// </summary>
    class HullPlaneEditorViewModel : ViewModelBase
    {
        private List<ConvexViewModel> _convexes;
        /// <summary>
        /// Gets or sets the convex view models.
        /// </summary>
        public List<ConvexViewModel> Convexes
        {
            get { return _convexes; }
            set { SetValue(ref _convexes, value); }
        }

        public HullPlaneEditorViewModel()
        {
            Convexes = new List<ConvexViewModel>();
        }
    }
}
