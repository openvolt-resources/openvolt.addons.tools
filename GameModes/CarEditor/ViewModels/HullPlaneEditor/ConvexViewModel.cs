﻿using OpenVolt.SDK.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor.ViewModels.HullPlaneEditor
{
    /// <summary>
    /// Convex view model.
    /// </summary>
    class ConvexViewModel : ViewModelBase
    {
        private List<PlaneViewModel> _planes;
        /// <summary>
        /// Gets or sets the plane view models.
        /// </summary>
        public List<PlaneViewModel> Planes
        {
            get { return _planes; }
            set { SetValue(ref _planes, value); }
        }

        private string _text;
        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        public string Text
        {
            get { return _text; }
            set { SetValue(ref _text, value); }
        }

        public ConvexViewModel()
        {
            Planes = new List<PlaneViewModel>();
        }
    }
}
