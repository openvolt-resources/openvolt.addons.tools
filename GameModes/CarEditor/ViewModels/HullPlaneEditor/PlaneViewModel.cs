﻿using OpenVolt.Interfaces.UI;
using OpenVolt.SDK.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor.ViewModels.HullPlaneEditor
{
    /// <summary>
    /// Plane view model.
    /// </summary>
    class PlaneViewModel : ViewModelBase
    {
        private UIEventHandlers.UIMouseEventHandler _showPlane;
        /// <summary>
        /// Gets or sets the delegate used to show the plane.
        /// </summary>
        public UIEventHandlers.UIMouseEventHandler ShowPlane
        {
            get { return _showPlane; }
            set { SetValue(ref _showPlane, value); }
        }

        private string _text;
        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        public string Text
        {
            get { return _text; }
            set { SetValue(ref _text, value); }
        }

    }
}
