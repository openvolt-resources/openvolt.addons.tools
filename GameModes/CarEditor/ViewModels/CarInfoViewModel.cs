﻿using OpenVolt.Interfaces.Car;
using OpenVolt.Interfaces.UI;
using OpenVolt.SDK.UI;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor.ViewModels
{
    class CarInfoViewModel : ViewModelBase
    {
        private string _previewFile;
        public string PreviewFile
        {
            get
            {
                return _previewFile;
            }
            set
            {
                SetValue(ref _previewFile, value);
            }
        }

        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                SetValue(ref _name, value);
            }
        }

        private UIEventHandlers.UIMouseEventHandler _loadCar;
        public UIEventHandlers.UIMouseEventHandler LoadCar
        {
            get
            {
                return _loadCar;
            }
            set
            {
                SetValue(ref _loadCar, value);
            }
        }

        private ICarParameters _parameters;
        public ICarParameters Parameters
        {
            get
            {
                return _parameters;
            }
            set
            {
                SetValue(ref _parameters, value);
            }
        }
    }
}
