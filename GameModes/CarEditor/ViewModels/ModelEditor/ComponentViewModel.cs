﻿using OpenVolt.Interfaces.UI;
using OpenVolt.SDK.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor.ViewModels.ModelEditor
{
    /// <summary>
    /// Car component view model.
    /// </summary>
    class ComponentViewModel : ViewModelBase
    {
        private string _name;
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { SetValue(ref _name, value); }
        }

        private UIEventHandlers.UIMouseEventHandler _loadModel;
        /// <summary>
        /// Gets or sets the delegate used to load a component model.
        /// </summary>
        public UIEventHandlers.UIMouseEventHandler LoadModel
        {
            get { return _loadModel; }
            set { SetValue(ref _loadModel, value); }
        }

        private UIEventHandlers.UIMouseEventHandler _moveModel;
        /// <summary>
        /// Gets or sets the delegate used to move a car component.
        /// </summary>
        public UIEventHandlers.UIMouseEventHandler MoveModel
        {
            get { return _moveModel; }
            set { SetValue(ref _moveModel, value); }
        }
    }
}
