﻿using OpenVolt.SDK.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor.ViewModels.ModelEditor
{
    /// <summary>
    /// Model editor view model.
    /// </summary>
    class ModelEditorViewModel : ViewModelBase
    {
        private List<ComponentViewModel> _components;
        /// <summary>
        /// Gets or sets the car component view models.
        /// </summary>
        public List<ComponentViewModel> Components
        {
            get { return _components; }
            set { SetValue(ref _components, value); }
        }

        public ModelEditorViewModel()
        {
            Components = new List<ComponentViewModel>();
        }
    }
}
