﻿using OpenVolt.Interfaces.UI;
using OpenVolt.SDK.UI;
using OpenVolt.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor.ViewModels
{
    class CarEditorViewModel : ViewModelBase
    {
        private UIEventHandlers.UIMouseEventHandler _showCarSelection;
        public UIEventHandlers.UIMouseEventHandler ShowCarSelection
        {
            get => _showCarSelection;
            set => SetValue(ref _showCarSelection, value);
        }

        private UIEventHandlers.UIMouseEventHandler _createNewCar;
        public UIEventHandlers.UIMouseEventHandler CreateNewCar
        {
            get => _createNewCar;
            set => SetValue(ref _createNewCar, value);
        }

        private UIEventHandlers.UIMouseEventHandler _takeControl;
        public UIEventHandlers.UIMouseEventHandler TakeControl
        {
            get => _takeControl;
            set => SetValue(ref _takeControl, value);
        }

        private UIEventHandlers.UIFocusEventHandler _filterCars;
        public UIEventHandlers.UIFocusEventHandler FilterCars
        {
            get => _filterCars;
            set => SetValue(ref _filterCars, value);
        }

        private UIEventHandlers.UIMouseEventHandler _aiControl;
        public UIEventHandlers.UIMouseEventHandler AIControl
        {
            get => _aiControl;
            set => SetValue(ref _aiControl, value);
        }

        private UIEventHandlers.UIMouseEventHandler _quitGame;
        public UIEventHandlers.UIMouseEventHandler QuitGame
        {
            get => _quitGame;
            set => SetValue(ref _quitGame, value);
        }

        public List<CarInfoViewModel> AllCars { get; set; }

        private List<CarInfoViewModel> _filteredCars;
        public List<CarInfoViewModel> FilteredCars
        {
            get => _filteredCars;
            set => SetValue(ref _filteredCars, value);
        }

        private UIEventHandlers.UIMouseEventHandler _openTab;
        public UIEventHandlers.UIMouseEventHandler OpenTab
        {
            get => _openTab;
            set => SetValue(ref _openTab, value);
        }

        private UIEventHandlers.UIMouseEventHandler _saveParameters;
        public UIEventHandlers.UIMouseEventHandler SaveParameters
        {
            get => _saveParameters;
            set => SetValue(ref _saveParameters, value);
        }

        private IUIElement _currentTab;
        public IUIElement CurrentTab
        {
            get => _currentTab;
            set => SetValue(ref _currentTab, value);
        }

        private float _tabWidth;
        public float TabWidth
        {
            get => _tabWidth;
            set => SetValue(ref _tabWidth, value);
        }

        public CarEditorViewModel()
        {
            FilteredCars = new List<CarInfoViewModel>();
            TabWidth = 150;
        }
    }
}
