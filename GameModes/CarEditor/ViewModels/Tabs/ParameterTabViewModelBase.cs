﻿using OpenVolt.Addons.Tools.GameModes.CarEditor.Classes;
using OpenVolt.Interfaces.UI;
using OpenVolt.Interfaces.UI.Elements;
using OpenVolt.SDK;
using OpenVolt.SDK.UI;
using OpenVolt.Shared;
using OpenVolt.Shared.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor.ViewModels.Tabs
{
    /// <summary>
    /// Parameter tab view model.
    /// </summary>
    /// <typeparam name="TParameters"></typeparam>
    abstract class ParameterTabViewModelBase<TParameters> : ViewModelBase
    {
        /// <summary>
        /// Gets or sets the current parameters.
        /// </summary>
        protected TParameters Parameters { get; set; }

        /// <summary>
        /// Gets or sets the entity parameters to show.
        /// </summary>
        protected List<EntityParameterInfo> ParametersToShow { get; set; }

        /// <summary>
        /// Gets or sets the delegate to call to make the entity process parameters.
        /// </summary>
        private Action TriggerProcessParameters { get; set; }

        public ParameterTabViewModelBase(TParameters parameters, Action processParameter = null)
        {
            Parameters = parameters;
            TriggerProcessParameters = processParameter;
            ParametersToShow = new List<EntityParameterInfo>();
        }

        /// <summary>
        /// Generates the UI of editable entity properties.
        /// </summary>
        /// <param name="tabElement"></param>
        public void ListProperties(IUIElement tabElement)
        {
            IUIStackPanel propertyWrapper = tabElement.GetElementByName<IUIStackPanel>("propertyWrapper");

            foreach (EntityParameterInfo paramInfo in ParametersToShow)
            {
                IUIElement row = CreateRow(paramInfo);
                row.Parent = propertyWrapper;
            }
        }

        /// <summary>
        /// Executes additional tasks after the value has been set.
        /// </summary>
        /// <param name="propertyName"></param>
        protected virtual void OnValueUpdated(string propertyName) { }

        /// <summary>
        /// Updates the parameter value.
        /// </summary>
        /// <param name="elementGetter"></param>
        /// <param name="elementSetter"></param>
        /// <param name="property"></param>
        private void UpdateParameterValue(Func<object> elementGetter, Action<object> elementSetter, string property)
        {
            Type destType = typeof(TParameters);
            PropertyInfo destProperty = destType.GetProperty(property);
            object value;

            XUIParserProxy.SetValue(Parameters, destProperty, elementGetter());
            TriggerProcessParameters?.Invoke();
            OnValueUpdated(property);

            value = destProperty.GetValue(Parameters);
            elementSetter(value == null ? string.Empty : value.ToString());
        }

        /// <summary>
        /// Creates a row from parameter info for the property list.
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        private IUIElement CreateRow(EntityParameterInfo info)
        {
            Type destType = typeof(TParameters);
            PropertyInfo destProperty = destType.GetProperty(info.Property);
            object value;

            IUIStackPanel stackPanel = UIProxy.CreateStackPanel();
            stackPanel.HorizontalAlignment = UIElementHorizontalAlignment.Stretch;
            stackPanel.AutoSize = true;

            IUILabel label = UIProxy.CreateLabel();
            label.Text = info.Text;
            label.HorizontalAlignment = UIElementHorizontalAlignment.Stretch;
            label.Parent = stackPanel;

            IUITextBox textBox = UIProxy.CreateTextBox();
            textBox.Width = 100;
            textBox.HorizontalAlignment = UIElementHorizontalAlignment.Stretch;
            textBox.Margin = new Rectangle(0, 0, 10, 0);
            textBox.OnFocusLost += (el, data) => UpdateParameterValue(() => textBox.Text, x => textBox.Text = x.ToString(), info.Property);
            textBox.Parent = stackPanel;
            value = destProperty.GetValue(Parameters);
            textBox.Text = value == null ? string.Empty : value.ToString();

            return stackPanel;
        }
    }
}
