﻿using OpenVolt.Interfaces.Car;
using OpenVolt.Interfaces.Car.Spring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor.ViewModels.Tabs
{
    /// <summary>
    /// Spring parameter collection tab view model.
    /// </summary>
    class SpringParameterCollectionTabViewModel : ParameterCollectionTabViewModelBase<SpringParameterTabViewModel, ISpringParameters>
    {
        protected override string BaseTabText
        {
            get { return "Spring"; }
        }

        protected override string TabParameterTemplateFile
        {
            get { return "car_editor/tabs/generic_parameter_tab.xui"; }
        }

        public SpringParameterCollectionTabViewModel(ICarEntity car) : base(car)
        {
        }

        protected override void CreateParameterTabs()
        {
            for (int i = 0; i < Car.Springs.Count; i++)
            {
                SpringParameterTabViewModel vm = new SpringParameterTabViewModel(Car.Parameters.SpringParameters[i], Car.Springs[i].TriggerProcessParameters);
                CreateTab(vm);
            }
        }
    }
}
