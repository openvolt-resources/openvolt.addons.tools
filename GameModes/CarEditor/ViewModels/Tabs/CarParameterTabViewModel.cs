﻿using OpenVolt.Addons.Tools.GameModes.CarEditor.Classes;
using OpenVolt.Interfaces.Car;
using OpenVolt.Interfaces.UI;
using OpenVolt.SDK;
using OpenVolt.SDK.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor.ViewModels.Tabs
{
    /// <summary>
    /// Car parameter tab view model.
    /// </summary>
    class CarParameterTabViewModel : ParameterTabViewModelBase<ICarParameters>
    {
        public CarParameterTabViewModel(ICarParameters parameters, Action processParameter) : base(parameters, processParameter)
        {
            ParametersToShow = new List<EntityParameterInfo>
            {
                new EntityParameterInfo { Property = nameof(ICarParameters.SteerRate), Text = "Steer Rate:" },
                new EntityParameterInfo { Property = nameof(ICarParameters.SteerMod), Text = "Steer Mod:" },
                new EntityParameterInfo { Property = nameof(ICarParameters.TopSpeed), Text = "Top Speed:" },
                new EntityParameterInfo { Property = nameof(ICarParameters.EngineRate), Text = "Engine Rate:" },
                new EntityParameterInfo { Property = nameof(ICarParameters.DownForceMod), Text = "Down Force Mod:" }
            };
        }
    }
}
