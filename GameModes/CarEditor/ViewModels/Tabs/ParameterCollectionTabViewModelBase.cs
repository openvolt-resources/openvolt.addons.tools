﻿using OpenVolt.Interfaces.Car;
using OpenVolt.Interfaces.UI;
using OpenVolt.SDK;
using OpenVolt.SDK.UI;
using OpenVolt.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor.ViewModels.Tabs
{
    /// <summary>
    /// Parameter collection tab view model.
    /// </summary>
    /// <typeparam name="TParameterTabViewModel"></typeparam>
    /// <typeparam name="TParameters"></typeparam>
    abstract class ParameterCollectionTabViewModelBase<TParameterTabViewModel, TParameters> : ViewModelBase
        where TParameterTabViewModel : ParameterTabViewModelBase<TParameters>
    {
        private List<TabViewModel> _tabs;
        /// <summary>
        /// Gets or sets the collection tab view models.
        /// </summary>
        public List<TabViewModel> Tabs
        {
            get
            {
                return _tabs;
            }
            set
            {
                SetValue(ref _tabs, value);
            }
        }

        private IUIElement _currentTab;
        /// <summary>
        /// Gets or sets the current opened tab.
        /// </summary>
        public IUIElement CurrentTab
        {
            get
            {
                return _currentTab;
            }
            set
            {
                SetValue(ref _currentTab, value);
            }
        }

        /// <summary>
        /// Gets or sets the base text of the parameter tab.
        /// </summary>
        protected abstract string BaseTabText { get; }

        /// <summary>
        /// Gets or sets the parameter tab template.
        /// </summary>
        protected abstract string TabParameterTemplateFile { get; }

        /// <summary>
        /// Gets or sets the car used to create parameter tab view models.
        /// </summary>
        protected ICarEntity Car { get; set; }

        /// <summary>
        /// Gets or sets the parameter tab view models.
        /// </summary>
        private List<TParameterTabViewModel> ParameterTabViewModels { get; set; }

        public ParameterCollectionTabViewModelBase(ICarEntity car)
        {
            Car = car;
            Tabs = new List<TabViewModel>();
            ParameterTabViewModels = new List<TParameterTabViewModel>();
            CreateParameterTabs();
        }

        /// <summary>
        /// Creates the parameter tab view models.
        /// </summary>
        protected abstract void CreateParameterTabs();

        /// <summary>
        /// Open the parameter tab from index.
        /// </summary>
        /// <param name="i"></param>
        private void ShowParameterTab(int i)
        {
            TParameterTabViewModel vm = ParameterTabViewModels[i];
            CurrentTab = XUIParserProxy.LoadTemplate(Paths.UI + TabParameterTemplateFile, vm);
            CurrentTab.OnInitialized += (el) => vm.ListProperties(el);
        }

        /// <summary>
        /// Creates a new collection tab from a parameter tab view model.
        /// </summary>
        /// <param name="vm"></param>
        protected void CreateTab(TParameterTabViewModel vm)
        {
            int id = ParameterTabViewModels.Count;
            Tabs.Add(new TabViewModel
            {
                ShowParameters = (el, data) => ShowParameterTab(id),
                Text = $"{(string.IsNullOrEmpty(BaseTabText) ? "" : BaseTabText + " ")}{ParameterTabViewModels.Count + 1}"
            });

            ParameterTabViewModels.Add(vm);
        }
    }
}
