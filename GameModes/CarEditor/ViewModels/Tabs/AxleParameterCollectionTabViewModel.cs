﻿using OpenVolt.Interfaces.Car;
using OpenVolt.Interfaces.Car.Axle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor.ViewModels.Tabs
{
    /// <summary>
    /// Axle parameter collection tab view model.
    /// </summary>
    class AxleParameterCollectionTabViewModel : ParameterCollectionTabViewModelBase<AxleParameterTabViewModel, IAxleParameters>
    {
        protected override string BaseTabText
        {
            get { return "Axle"; }
        }

        protected override string TabParameterTemplateFile
        {
            get { return "car_editor/tabs/generic_parameter_tab.xui"; }
        }

        public AxleParameterCollectionTabViewModel(ICarEntity car) : base(car)
        {
        }

        protected override void CreateParameterTabs()
        {
            for (int i = 0; i < Car.Wheels.Count; i++)
            {
                AxleParameterTabViewModel vm = new AxleParameterTabViewModel(Car.Parameters.AxleParameters[i], Car.Axles[i].TriggerProcessParameters);
                CreateTab(vm);
            }
        }
    }
}
