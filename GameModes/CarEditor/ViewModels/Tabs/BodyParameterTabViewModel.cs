﻿using OpenVolt.Addons.Tools.GameModes.CarEditor.Classes;
using OpenVolt.Interfaces.Physics.Body;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor.ViewModels.Tabs
{
    /// <summary>
    /// Body parameter tab view model.
    /// </summary>
    class BodyParameterTabViewModel : ParameterTabViewModelBase<IBodyParameters>
    {
        private Action RefreshCoM { get; set; }

        public BodyParameterTabViewModel(IBodyParameters parameters, Action processParameter, Action refreshCoM) : base(parameters, processParameter)
        {
            RefreshCoM = refreshCoM;

            ParametersToShow = new List<EntityParameterInfo>
            {
                new EntityParameterInfo { Property = nameof(IBodyParameters.CenterOfMassOffset), Text = "Center Of Mass Offset:" },
                new EntityParameterInfo { Property = nameof(IBodyParameters.AngularResistance), Text = "Angular Resistance:" },
                new EntityParameterInfo { Property = nameof(IBodyParameters.AngularResistanceMod), Text = "Angular Resistance Mod:" }
            };
        }

        protected override void OnValueUpdated(string propertyName)
        {
            if (propertyName == nameof(IBodyParameters.CenterOfMassOffset))
                RefreshCoM();
        }
    }
}
