﻿using OpenVolt.Addons.Tools.GameModes.CarEditor.Classes;
using OpenVolt.Interfaces.Car;
using OpenVolt.Interfaces.Car.Wheel;
using OpenVolt.Interfaces.UI;
using OpenVolt.SDK;
using OpenVolt.SDK.UI;
using OpenVolt.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor.ViewModels.Tabs
{
    /// <summary>
    /// Wheel parameter collection tab view model.
    /// </summary>
    class WheelParameterCollectionTabViewModel : ParameterCollectionTabViewModelBase<WheelParameterTabViewModel, IWheelParameters>
    {
        protected override string BaseTabText
        {
            get { return "Wheel"; }
        }

        protected override string TabParameterTemplateFile
        {
            get { return "car_editor/tabs/generic_parameter_tab.xui"; }
        }

        public WheelParameterCollectionTabViewModel(ICarEntity car) : base(car)
        {
        }

        protected override void CreateParameterTabs()
        {
            for (int i = 0; i < Car.Wheels.Count; i++)
            {
                WheelParameterTabViewModel vm = new WheelParameterTabViewModel(Car.Parameters.WheelParameters[i], Car.Wheels[i].TriggerProcessParameters);
                CreateTab(vm);
            }
        }
    }
}
