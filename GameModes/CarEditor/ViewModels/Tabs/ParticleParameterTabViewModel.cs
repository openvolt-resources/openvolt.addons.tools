﻿using OpenVolt.Addons.Tools.GameModes.CarEditor.Classes;
using OpenVolt.Interfaces.Physics.Body;
using OpenVolt.Interfaces.Physics.Particle;
using OpenVolt.Interfaces.UI;
using OpenVolt.Interfaces.UI.Elements;
using OpenVolt.SDK;
using OpenVolt.Shared;
using OpenVolt.Shared.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor.ViewModels.Tabs
{
    /// <summary>
    /// Particle parameter tab view model.
    /// </summary>
    class ParticleParameterTabViewModel : ParameterTabViewModelBase<IParticleParameters>
    {
        public ParticleParameterTabViewModel(IParticleParameters parameters, Action processParameter) : base(parameters, processParameter)
        {
            ParametersToShow = new List<EntityParameterInfo>
            {
                new EntityParameterInfo { Property = nameof(IParticleParameters.Mass), Text = "Mass:" },
                new EntityParameterInfo { Property = nameof(IParticleParameters.Hardness), Text = "Hardness:" },
                new EntityParameterInfo { Property = nameof(IParticleParameters.Resistance), Text = "Resistance:" },
                new EntityParameterInfo { Property = nameof(IParticleParameters.Grip), Text = "Grip:" },
                new EntityParameterInfo { Property = nameof(IParticleParameters.StaticFriction), Text = "Static Friction:" },
                new EntityParameterInfo { Property = nameof(IParticleParameters.KineticFriction), Text = "Kinetic Friction:" },
                new EntityParameterInfo { Property = nameof(IParticleParameters.Gravity), Text = "Gravity:" }
            };
        }
    }
}
