﻿using OpenVolt.Addons.Tools.GameModes.CarEditor.Classes;
using OpenVolt.Interfaces.Car.Spring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor.ViewModels.Tabs
{
    /// <summary>
    /// Spring parameter tab view model.
    /// </summary>
    class SpringParameterTabViewModel : ParameterTabViewModelBase<ISpringParameters>
    {
        public SpringParameterTabViewModel(ISpringParameters parameters, Action processParameter = null) : base(parameters, processParameter)
        {
            ParametersToShow = new List<EntityParameterInfo>
            {
                new EntityParameterInfo { Property = nameof(ISpringParameters.Stiffness), Text = "Stiffness:" },
                new EntityParameterInfo { Property = nameof(ISpringParameters.Damping), Text = "Damping:" },
                new EntityParameterInfo { Property = nameof(ISpringParameters.Restitution), Text = "Restitution:" },
                new EntityParameterInfo { Property = nameof(ISpringParameters.Offset), Text = "Offset:" },
                new EntityParameterInfo { Property = nameof(ISpringParameters.Length), Text = "Length:" }
            };
        }
    }
}
