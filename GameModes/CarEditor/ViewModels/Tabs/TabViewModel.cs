﻿using OpenVolt.Interfaces.UI;
using OpenVolt.SDK.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor.ViewModels.Tabs
{
    /// <summary>
    /// Parameter tab view model.
    /// </summary>
    class TabViewModel : ViewModelBase
    {
        /// <summary>
        /// Gets or sets the delegate used to show the tab.
        /// </summary>
        public UIEventHandlers.UIMouseEventHandler ShowParameters { get; set; }

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        public string Text { get; set; }
    }
}
