﻿using OpenVolt.Addons.Tools.GameModes.CarEditor.Classes;
using OpenVolt.Interfaces.Car.Axle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor.ViewModels.Tabs
{
    /// <summary>
    /// Axle parameter tab view model.
    /// </summary>
    class AxleParameterTabViewModel : ParameterTabViewModelBase<IAxleParameters>
    {
        public AxleParameterTabViewModel(IAxleParameters parameters, Action processParameter = null) : base(parameters, processParameter)
        {
            ParametersToShow = new List<EntityParameterInfo>
            {
                new EntityParameterInfo { Property = nameof(IAxleParameters.Offset), Text = "Offset:" },
                new EntityParameterInfo { Property = nameof(IAxleParameters.Length), Text = "Length:" }
            };
        }
    }
}
