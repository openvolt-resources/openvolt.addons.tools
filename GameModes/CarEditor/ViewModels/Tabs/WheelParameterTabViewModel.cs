﻿using OpenVolt.Addons.Tools.GameModes.CarEditor.Classes;
using OpenVolt.Interfaces.Car;
using OpenVolt.Interfaces.Car.Wheel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor.ViewModels.Tabs
{
    /// <summary>
    /// Wheel parameter tab view model.
    /// </summary>
    class WheelParameterTabViewModel : ParameterTabViewModelBase<IWheelParameters>
    {
        public WheelParameterTabViewModel(IWheelParameters parameters, Action processParameter) : base(parameters, processParameter)
        {
            ParametersToShow = new List<EntityParameterInfo>
            {
                new EntityParameterInfo{ Property = nameof(IWheelParameters.FixingPointOffset), Text = "Fixing Point Offset:" },
                new EntityParameterInfo{ Property = nameof(IWheelParameters.CentreOffset), Text = "Centre Offset:" },
                new EntityParameterInfo{ Property = nameof(IWheelParameters.Mass), Text = "Mass:" },
                new EntityParameterInfo{ Property = nameof(IWheelParameters.Radius), Text = "Radius:" },
                new EntityParameterInfo{ Property = nameof(IWheelParameters.Gravity), Text = "Gravity:" },
                new EntityParameterInfo{ Property = nameof(IWheelParameters.StaticFriction), Text = "Static Friction:" },
                new EntityParameterInfo{ Property = nameof(IWheelParameters.KineticFriction), Text = "Kinetic Friction:" },
                new EntityParameterInfo{ Property = nameof(IWheelParameters.AxleFriction), Text = "Axle Friction:" },
                new EntityParameterInfo{ Property = nameof(IWheelParameters.SteerRatio), Text = "Steer Ratio:" },
                new EntityParameterInfo{ Property = nameof(IWheelParameters.EngineRatio), Text = "Engine Ratio:" },
                new EntityParameterInfo{ Property = nameof(IWheelParameters.Grip), Text = "Grip:" },
                new EntityParameterInfo{ Property = nameof(IWheelParameters.MaxPosition), Text = "Max Position:" },
                new EntityParameterInfo{ Property = nameof(IWheelParameters.SkidWidth), Text = "Skid Width:" }
            };
        }
    }
}
