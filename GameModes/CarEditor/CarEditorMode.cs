﻿using OpenVolt.Addons.Tools.GameModes.CarEditor.Classes;
using OpenVolt.Addons.Tools.GameModes.CarEditor.Enums;
using OpenVolt.Addons.Tools.GameModes.CarEditor.IO;
using OpenVolt.Addons.Tools.GameModes.CarEditor.ViewModels;
using OpenVolt.Addons.Tools.GameModes.CarEditor.ViewModels.Tabs;
using OpenVolt.Interfaces.Car;
using OpenVolt.Interfaces.Engine;
using OpenVolt.Interfaces.GameMode;
using OpenVolt.Interfaces.Input;
using OpenVolt.Interfaces.IO;
using OpenVolt.Interfaces.Module;
using OpenVolt.Interfaces.Physics.Particle;
using OpenVolt.Interfaces.UI;
using OpenVolt.Interfaces.UI.Elements;
using OpenVolt.SDK;
using OpenVolt.SDK.GameMode;
using OpenVolt.Services;
using OpenVolt.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor
{
    /// <summary>
    /// Car editor game mode.
    /// </summary>
    [GameMode("Car editor")]
    class CarEditorMode : GameModeBase, IGameModeWithAIModule, IGameModeWithData, IGameMode
    {
        /// <summary>
        /// Gets or sets the AI module instance.
        /// </summary>
        public IAIModule AIModule { get; private set; }

        /// <summary>
        /// Gets the car editor data.
        /// </summary>
        IGameModeData IGameModeWithData.Data
        {
            get
            {
                return Data;
            }
        }

        /// <summary>
        /// Gets the controller.
        /// </summary>
        IController IGameMode.GameModeController
        {
            get
            {
                return GameModeController;
            }
        }

        /// <summary>
        /// Gets or sets the main controller.
        /// </summary>
        private new CarEditorController GameModeController { get; set; }

        /// <summary>
        /// Gets or sets the base level file name to help accessing the files.
        /// </summary>
        private string LevelFileNameBase { get; set; }

        /// <summary>
        /// Gets or sets the base directory from where to load UI templates.
        /// </summary>
        private string UIBaseDirectory { get; set; }

        /// <summary>
        /// Gets or sets the view model instance.
        /// </summary>
        private CarEditorViewModel ViewModel { get; set; }

        /// <summary>
        /// Gets or sets the car module instance.
        /// </summary>
        private ICarModule CarModule { get; set; }

        /// <summary>
        /// Gets or sets the car editor data.
        /// </summary>
        private CarEditorData Data { get; set; }

        public CarEditorMode()
        {
            // Create view model
            ViewModel = new CarEditorViewModel();

            // And bind view model delegates
            ViewModel.ShowCarSelection = (el, data) => ShowCarSelectionScreen();
            ViewModel.OpenTab = (el, data) => OpenTab(el.Parameter.ToString());
            ViewModel.TakeControl = (el, data) => TakeControl();
            ViewModel.AIControl = (el, data) => AIControl();
            ViewModel.QuitGame = (el, data) => SceneProxy.QuitGame();
            ViewModel.FilterCars = (el, data) => FilterCarList(((IUITextBox)el).Text);

            // Set the level to load
            LevelFile = Paths.LEVELS + "nhood1/nhood1.inf";

            // New data instance
            Data = new CarEditorData();

            // Register initialization method
            OnInitialize += InitializeAction;
        }

        /// <summary>
        /// Initializes the car editor.
        /// </summary>
        private void InitializeAction()
        {
            // Load required modules
            AIModule = ModuleProxy.GetAIModule();
            CarModule = ModuleProxy.GetCarModule();

            // Create car editor controller
            GameModeController = new CarEditorController();

            // Set base paths
            LevelFileNameBase = LevelDirectoryFullName + "/" + LevelName;
            UIBaseDirectory = Paths.UI + "car_editor/";

            // Add load stages
            AddLoadStage(new GameModeLoadStage(AILoadStage, "Loading AI..."));
            AddLoadStage(new GameModeLoadStage(FinalLoadStage, "Finalizing..."));
        }

        /// <summary>
        /// Does the last load tasks.
        /// </summary>
        /// <param name="next"></param>
        private void FinalLoadStage(Action next)
        {
            CreateCameraTargetParticle();
            ShowCarSelectionScreen();
            Data.TrackedPlayerCarController = ControllerProxy.CreateTrackedPlayerCarController();
            Data.AICarController = ControllerProxy.CreateAICarController();
            next();
        }

        /// <summary>
        /// Loads AI data.
        /// </summary>
        /// <param name="next"></param>
        private void AILoadStage(Action next)
        {
            // Create promise
            IDefaultEnginePromise aiPromise = AIModule.LoadAIGraphAsync(LevelFileNameBase + ".fan", LevelFileNameBase + ".taz", World.Parameters.StartPosition);
            // Set callback
            aiPromise.OnResolve += (data) => next();
            // Call the promise
            aiPromise.Run();
        }

        /// <summary>
        /// Creates a particle to allow setting custom camera position.
        /// </summary>
        private void CreateCameraTargetParticle()
        {
            // Create particle
            IParticleParameters parameters = ParticleEntityProxy.CreateParameters();
            Data.TargetParticle = ParticleEntityProxy.Create();
            Data.TargetParticle.Load(parameters);

            // Set the default position to the level start position
            Data.TargetParticle.SetPosition(World.Parameters.StartPosition, World.Parameters.StartRotation);
        }

        /// <summary>
        /// Opens the car selection screen.
        /// </summary>
        private void ShowCarSelectionScreen()
        {
            // Show a loading screen overlay
            IUIElement loadingOverlay = XUIParserProxy.LoadTemplate(UIBaseDirectory + "loading_overlay.xui", ViewModel);

            // Be sure the camera come back to the start positon
            Data.TargetParticle.SetPosition(World.Parameters.StartPosition, World.Parameters.StartRotation);
            MainCamera.FollowParticle(Data.TargetParticle);

            // Destroy previous screen
            Data.CurrentScreen?.Remove();

            // Reset car list
            ViewModel.FilteredCars = new List<CarInfoViewModel>();

            // Load the car selection screen
            Data.CurrentScreen = XUIParserProxy.LoadTemplate(UIBaseDirectory + "car_selection_screen.xui", ViewModel);

            // Load car list
            IDefaultEnginePromise getCarListPromise = GetCarList();
            getCarListPromise.OnResolve += (data) =>
            {
                // Destroy loading overlay
                loadingOverlay.Remove();

                // Set car list
                ViewModel.AllCars = (List<CarInfoViewModel>)data;
                ViewModel.FilteredCars = (List<CarInfoViewModel>)data;
            };
            getCarListPromise.Run();
        }

        /// <summary>
        /// Loads a car.
        /// </summary>
        /// <param name="parameters"></param>
        private void LoadCar(ICarParameters parameters)
        {
            IEnginePromise<ICarEntity, object> carLoadPromise = CarModule.LoadCarAsync((ICarParameters)parameters.Clone(), World.Parameters.StartPosition, World.Parameters.StartRotation);
            carLoadPromise.OnResolve += (car) =>
            {
                // If a car was loaded previously
                if (Data.CurrentCar != null)
                {
                    // Unpocess it
                    Data.TrackedPlayerCarController.Unpocess();
                    Data.AICarController.Unpocess();

                    // And destroy it
                    Data.CurrentCar.Destroy();
                }

                // Pocess the new car
                Data.CurrentCar = car;
                Data.TrackedPlayerCarController.Pocess(car);

                // Create view models
                Data.CarParameterTabViewModel = new CarParameterTabViewModel(car.Parameters, car.TriggerProcessParameters);
                Data.ParticleParameterTabViewModel = new ParticleParameterTabViewModel(car.Parameters.BodyParameters.ParticleParameters, car.Body.Centre.TriggerProcessParameters);
                Data.BodyParameterTabViewModel = new BodyParameterTabViewModel(car.Parameters.BodyParameters, car.Body.TriggerProcessParameters, car.Body.RefreshCenterOfMass);
                Data.WheelParameterCollectionTabViewModel = new WheelParameterCollectionTabViewModel(car);
                Data.AxleParameterCollectionTabViewModel = new AxleParameterCollectionTabViewModel(car);
                Data.SpringParameterCollectionTabViewModel = new SpringParameterCollectionTabViewModel(car);

                // Create editor helpers
                Data.HullPlaneEditor?.Dispose();
                Data.ModelEditor?.Dispose();
                Data.HullPlaneEditor = new HullPlaneEditor(Data.CurrentCar, GameModeController, Data.TargetParticle);
                Data.ModelEditor = new ModelEditor(Data.CurrentCar, GameModeController, Data.TargetParticle);
                Data.RvCarParameterExportHelper = new RvCarParameterExportHelper(car.Parameters);

                // Set the new save action for view model
                ViewModel.SaveParameters = (el, data) => Data.RvCarParameterExportHelper.Save(FileUtils.GetDirectoryFullName(car.Parameters.File) + "/new_parameters.txt");

                // Tell the camera to follow the car
                MainCamera.FollowParticle(car.Body.Centre);

                // Then show car editor
                ShowCarEditor();
            };
            carLoadPromise.Run();
        }

        /// <summary>
        /// Opens the car editor.
        /// </summary>
        private void ShowCarEditor()
        {
            // Destroy the previous screen
            Data.CurrentScreen.Remove();

            // Load the car editor screen
            Data.CurrentScreen = XUIParserProxy.LoadTemplate(UIBaseDirectory + "car_editor_screen.xui", ViewModel);
        }

        /// <summary>
        /// Opens a car editor tab.
        /// </summary>
        /// <param name="tabName"></param>
        private void OpenTab(string tabName)
        {
            TabName newTabName = TabName.None;

            // Correctly close the hull editor
            if(Data.CurrentTabName == TabName.HullEditor)
            {
                // Unselect planes
                Data.HullPlaneEditor.SelectPlane(-1, -1);
                // Unregister editor methods
                OnUpdate -= Data.HullPlaneEditor.MovePlane;
                // Make planes invisible
                Data.HullPlaneEditor.Visible = false;
                // Enable the interactive mode again
                EnableInteractiveMode();
                // Unfreeze the car
                UnfreezeCar();
            }
            // Correctly close the model editor
            else if (Data.CurrentTabName == TabName.ModelEditor)
            {
                // Unregister editor methods
                OnUpdate -= Data.ModelEditor.MoveModel;
                //OnPhysicsUpdate -= ForceParticleToFollowCar;
                // Restore car model
                Data.ModelEditor.SetOpacity(1.0f);
                Data.ModelEditor.Visible = false;
                // Enable the interactive mode again
                EnableInteractiveMode();
                UnfreezeCar();
            }

            // Try to get the tab name
            Enum.TryParse(tabName, true, out newTabName);
            Data.CurrentTabName = newTabName;

            if (Data.CurrentTabName == TabName.CarParameters)
            {
                ViewModel.CurrentTab = XUIParserProxy.LoadTemplate(UIBaseDirectory + "tabs/generic_parameter_tab.xui", Data.CarParameterTabViewModel);
                ViewModel.CurrentTab.OnInitialized += (el) => Data.CarParameterTabViewModel.ListProperties(el);
            }
            else if(Data.CurrentTabName == TabName.ParticleParameters)
            {
                ViewModel.CurrentTab = XUIParserProxy.LoadTemplate(UIBaseDirectory + "tabs/generic_parameter_tab.xui", Data.ParticleParameterTabViewModel);
                ViewModel.CurrentTab.OnInitialized += (el) => Data.ParticleParameterTabViewModel.ListProperties(el);
            }
            else if (Data.CurrentTabName == TabName.BodyParameters)
            {
                ViewModel.CurrentTab = XUIParserProxy.LoadTemplate(UIBaseDirectory + "tabs/generic_parameter_tab.xui", Data.BodyParameterTabViewModel);
                ViewModel.CurrentTab.OnInitialized += (el) => Data.BodyParameterTabViewModel.ListProperties(el);
            }
            else if (Data.CurrentTabName == TabName.WheelParameters)
            {
                ViewModel.CurrentTab = XUIParserProxy.LoadTemplate(UIBaseDirectory + "tabs/generic_parameter_collection_tab.xui", Data.WheelParameterCollectionTabViewModel);
            }
            else if (Data.CurrentTabName == TabName.SpringParameters)
            {
                ViewModel.CurrentTab = XUIParserProxy.LoadTemplate(UIBaseDirectory + "tabs/generic_parameter_collection_tab.xui", Data.SpringParameterCollectionTabViewModel);
            }
            else if (Data.CurrentTabName == TabName.AxleParameters)
            {
                ViewModel.CurrentTab = XUIParserProxy.LoadTemplate(UIBaseDirectory + "tabs/generic_parameter_collection_tab.xui", Data.AxleParameterCollectionTabViewModel);
            }
            else if(Data.CurrentTabName == TabName.EditorMenu)
            {
                ViewModel.CurrentTab = XUIParserProxy.LoadTemplate(UIBaseDirectory + "tabs/editor_menu.xui", ViewModel);
            }
            else if (Data.CurrentTabName == TabName.ModelEditor)
            {
                EnableViewMode();
                FreezeCar();
                //OnPhysicsUpdate += ForceParticleToFollowCar;
                OnUpdate += Data.ModelEditor.MoveModel;
                Data.ModelEditor.Visible = true;
                ViewModel.CurrentTab = XUIParserProxy.LoadTemplate(UIBaseDirectory + "tabs/model_editor.xui", Data.ModelEditor.ViewModel);
            }
            else if (Data.CurrentTabName == TabName.HullEditor)
            {
                EnableViewMode();
                FreezeCar();
                OnPhysicsUpdate += Data.HullPlaneEditor.MovePlane;
                Data.HullPlaneEditor.Visible = true;
                ViewModel.CurrentTab = XUIParserProxy.LoadTemplate(UIBaseDirectory + "tabs/hull_editor.xui", Data.HullPlaneEditor.ViewModel);
            }
            else
            {
                ViewModel.CurrentTab = null;
            }
        }

        /// <summary>
        /// Enables the interactive mode.
        /// </summary>
        private void EnableInteractiveMode()
        {
            if (Data.CurrentCar == null)
                return;

            // Only rotate the camera around Y axis
            MainCamera.Parameters.VerticalRotationOnly = true;
            // Unregister the move camera method
            OnUpdate -= MoveCamera;

            // Got to the car position
            Data.TargetParticle.SetPosition(Data.CurrentCar.Body.Centre.Data.Position, Data.CurrentCar.Body.Centre.Data.WorldMatrix);
            // Tells the camera to follow the car
            MainCamera.FollowParticle(Data.CurrentCar.Body.Centre);
        }

        /// <summary>
        /// Freezes the car.
        /// </summary>
        private void FreezeCar()
        {
            // Backup car handlers
            Data.BodyOnParticleMove = Data.CurrentCar.Body.Centre.OnParticleMove;
            Data.BodyOnAfterCollisionProcessing = Data.CurrentCar.Body.Centre.OnAfterCollisionProcessing;
            Data.CarOnBeforeBodyMoved = Data.CurrentCar.Body.OnBeforeBodyMoved;
            Data.CarOnAfterBodyMoved = Data.CurrentCar.Body.OnAfterBodyMoved;
            Data.CarOnBeforeCollisionProcessing = Data.CurrentCar.Body.OnBeforeCollisionProcessing;
            Data.CarOnAfterCollisionProcessing = Data.CurrentCar.Body.OnAfterCollisionProcessing;
            Data.CarOnUpdateTick = Data.CurrentCar.Body.OnUpdateTick;

            // Remove car handlers
            Data.CurrentCar.Body.Centre.OnParticleMove = null;
            Data.CurrentCar.Body.Centre.OnAfterCollisionProcessing = null;
            Data.CurrentCar.Body.OnBeforeBodyMoved = null;
            Data.CurrentCar.Body.OnAfterBodyMoved = null;
            Data.CurrentCar.Body.OnBeforeCollisionProcessing = null;
            Data.CurrentCar.Body.OnAfterCollisionProcessing = null;
            Data.CurrentCar.Body.OnUpdateTick = null;
        }

        /// <summary>
        /// Unfreezes the car.
        /// </summary>
        private void UnfreezeCar()
        {
            // Restore car handlers
            Data.CurrentCar.Body.Centre.OnParticleMove += Data.BodyOnParticleMove;
            Data.CurrentCar.Body.Centre.OnAfterCollisionProcessing += Data.BodyOnAfterCollisionProcessing;
            Data.CurrentCar.Body.OnBeforeBodyMoved += Data.CarOnBeforeBodyMoved;
            Data.CurrentCar.Body.OnAfterBodyMoved += Data.CarOnAfterBodyMoved;
            Data.CurrentCar.Body.OnBeforeCollisionProcessing += Data.CarOnBeforeCollisionProcessing;
            Data.CurrentCar.Body.OnAfterCollisionProcessing += Data.CarOnAfterCollisionProcessing;
            Data.CurrentCar.Body.OnUpdateTick += Data.CarOnUpdateTick;
        }

        /// <summary>
        /// Enables the view mode.
        /// </summary>
        private void EnableViewMode()
        {
            if (Data.CurrentCar == null)
                return;

            // Allow to rotate around all axes
            MainCamera.Parameters.VerticalRotationOnly = false;
            // Register the camera rotation method
            OnUpdate += MoveCamera;

            // Go to the last car position
            Data.TargetParticle.SetPosition(Data.CurrentCar.Body.Centre.Data.Position, Matrix.UnitMatrix);
            // Tell the camera to follow the particle
            MainCamera.FollowParticle(Data.TargetParticle);

            // Set the rotation to the last saved values
            RotateTargetParticle();
        }

        /// <summary>
        /// Lets the AI control the car.
        /// </summary>
        private void AIControl()
        {
            if (Data.CurrentCar == null)
                return;

            Data.TrackedPlayerCarController.Unpocess();
            Data.AICarController.Pocess(Data.CurrentCar);
        }

        /// <summary>
        /// Lets the player control the car.
        /// </summary>
        private void TakeControl()
        {
            if (Data.CurrentCar == null)
                return;

            Data.AICarController.Unpocess();
            Data.TrackedPlayerCarController.Pocess(Data.CurrentCar);
        }

        /// <summary>
        /// Filters the car list with the given keyword.
        /// </summary>
        /// <param name="filter"></param>
        private void FilterCarList(string filter)
        {
            ViewModel.FilteredCars = ViewModel.AllCars.Where(x => x.Name.ToLower().Contains(filter.ToLower())).ToList();
        }

        /// <summary>
        /// Gets the cars available in the car directory.
        /// </summary>
        /// <returns></returns>
        private IDefaultEnginePromise GetCarList()
        {
            return EnginePromiseProxy.Create((res, rej) =>
            {
                List<CarInfoViewModel> result = new List<CarInfoViewModel>();
                ICarLoadHelper carLoadHelper = Service.GetInstance<FileLoaderService>().Get<ICarLoadHelper>();
                IDefaultEnginePromise carInfoPromise = EnginePromiseProxy.CreateAsyncForEachLoop(
                    (res2) =>
                    {
                        // Get the car folder list
                        res2(Directory.EnumerateDirectories(Paths.CARS));
                    },
                    (carFolder, res2) =>
                    {
                        // Check if the car folder contains a parameter file
                        if (!File.Exists(carFolder + "/parameters.txt"))
                        {
                            res2(null);
                            return;
                        }

                        CarInfoViewModel carInfo = new CarInfoViewModel();
                        // Load the car parameters
                        carInfo.Parameters = carLoadHelper.Load(carFolder + "/parameters.txt");
                        // Get its name
                        carInfo.Name = carInfo.Parameters.Name;
                        // Set the thumbnail (currently the car texture, quite shitty but does the job for now...)
                        carInfo.PreviewFile = carInfo.Parameters.BodyParameters.Materials.FirstOrDefault().Value.TextureFile;
                        // Set the car load method
                        carInfo.LoadCar = (el, data) => LoadCar((ICarParameters)el.Parameter);
                        // Add the car to the result list
                        result.Add(carInfo);
                        res2(null);
                    }
                );

                carInfoPromise.OnResolve += (data) => res(result);
                carInfoPromise.Run();
            });
        }

        /// <summary>
        /// Forces the camera target particle to follow the car.
        /// </summary>
        private void ForceParticleToFollowCar()
        {
            Data.TargetParticle.SetPosition(Data.CurrentCar.Body.Centre.Data.Position, Data.TargetParticle.Data.WorldMatrix);
        }

        /// <summary>
        /// Moves the camera.
        /// </summary>
        private void MoveCamera()
        {
            if (GameModeController.InputManager.IsActionActive(EditorAction.Rotate))
                RotateTargetParticle();
        }

        /// <summary>
        /// Rotates the camera target particle.
        /// </summary>
        private void RotateTargetParticle()
        {
            Vector3 look = new Vector3();

            // Add slide delta angle around Y axis
            Data.LookAngleX -= GameModeController.InputManager.GetActionValue(EditorAction.SlideX) * 2;
            // And wrap it for a 360 degree rotation
            Data.LookAngleX = Data.LookAngleX.Wrap(-180, 180);

            // Add slide delta to angle around X axis
            Data.LookAngleY += GameModeController.InputManager.GetActionValue(EditorAction.SlideY) * 2;
            Data.LookAngleY = Data.LookAngleY.Clamp(-90.0f, 90.0f);

            // Create the look vector from angles
            look = new Vector3((float)Math.Cos(Data.LookAngleX.ToRadians()), (float)Math.Sin(Data.LookAngleY.ToRadians()), (float)Math.Sin(Data.LookAngleX.ToRadians()));

            // Tell the particle to look in the created look vector direction
            Data.TargetParticle.SetPosition(Data.TargetParticle.Data.Position, Matrix.FromLook(look.Normalized));
        }
    }
}
