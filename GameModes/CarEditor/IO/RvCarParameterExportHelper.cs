﻿using OpenVolt.Interfaces.Car;
using OpenVolt.Shared;
using OpenVolt.Shared.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor.IO
{
    /// <summary>
    /// Helper to export parameters to Re-Volt.
    /// </summary>
    class RvCarParameterExportHelper
    {
        /// <summary>
        /// Gets or sets the content of the text file.
        /// </summary>
        private string Content { get; set; }

        /// <summary>
        /// Gets or sets the car parameters to export.
        /// </summary>
        private ICarParameters Parameters { get; set; }

        /// <summary>
        /// Handlers for custom type string conversion.
        /// </summary>
        private static readonly Dictionary<Type, Func<object, string>> SpecificTypeHandlers = new Dictionary<Type, Func<object, string>>
        {
            { typeof(Matrix), MatrixHandler },
            { typeof(Vector3), VectorHandler },
            { typeof(CarClass), EnumHandler },
            { typeof(CarRating), EnumHandler }
        };

        /// <summary>
        /// List of exported types.
        /// </summary>
        private static readonly HashSet<Type> SerializedTypes = new HashSet<Type>
        {
            typeof(int),
            typeof(float),
            typeof(bool),
            typeof(string),
            typeof(Vector3),
            typeof(Matrix),
            typeof(CarClass),
            typeof(CarRating)
        };

        public RvCarParameterExportHelper(ICarParameters parameters)
        {
            Parameters = parameters;
        }

        /// <summary>
        /// Exports the current car parameters to a Re-Volt parameter text file.
        /// </summary>
        /// <param name="file"></param>
        public void Save(string file)
        {
            // Load the Re-Volt parameter template
            Content = File.ReadAllText(Paths.DATA + "car_editor/Parameters.txt");

            // Auto-fill info with the car parameter instance
            FillInfoFromObject(Parameters);
            FillInfoFromObject(Parameters.AerialParameters);
            FillInfoFromObject(Parameters.AIParameters);
            FillInfoFromObject(Parameters.BodyParameters);
            FillInfoFromObject(Parameters.BodyParameters.ParticleParameters);
            FillInfoFromObject(Parameters.SpinnerParameters);

            for (int i = 0; i < 4; i++)
            {
                FillInfoFromObject(Parameters.AxleParameters[i]);
                FillInfoFromObject(Parameters.SpringParameters[i]);
                FillInfoFromObject(Parameters.WheelParameters[i]);
                FillInfoFromObject(Parameters.PinParameters[i]);
            }

            // Fill calculated info
            FillCalculatedInfo();

            // Write the file
            File.WriteAllText(file, Content);
        }

        /// <summary>
        /// Fills the remaining placeholders with the calculated info.
        /// </summary>
        private void FillCalculatedInfo()
        {
            // Gather model files
            List<string> modelFiles = new List<string>();

            modelFiles.Add(Parameters.BodyParameters.ModelFile);

            for (int i = 0; i < 4; i++)
            {
                modelFiles.Add(Parameters.AxleParameters[i].ModelFile);
                modelFiles.Add(Parameters.SpringParameters[i].ModelFile);
                modelFiles.Add(Parameters.WheelParameters[i].ModelFile);
            }

            // Filter and only get the different model files
            modelFiles = modelFiles.Distinct().Where(x => !string.IsNullOrEmpty(x)).ToList();

            // Fill the model array
            for(int i = 0; i < 17; i++)
            {
                if(i < modelFiles.Count)
                    FillFirstPlaceholder(modelFiles[i], "#ModelFiles");
                else
                    FillFirstPlaceholder("NONE", "#ModelFiles");
            }

            // Set the model to use for each car component
            FillFirstPlaceholder(modelFiles.IndexOf(Parameters.BodyParameters.ModelFile), "#BodyModel");

            for (int i = 0; i < 4; i++)
            {
                FillFirstPlaceholder(modelFiles.IndexOf(Parameters.AxleParameters[i].ModelFile), "#AxleModel");
                FillFirstPlaceholder(modelFiles.IndexOf(Parameters.SpringParameters[i].ModelFile), "#SpringModel");
                FillFirstPlaceholder(modelFiles.IndexOf(Parameters.WheelParameters[i].ModelFile), "#WheelModel");
                FillFirstPlaceholder(modelFiles.IndexOf(Parameters.PinParameters[i].ModelFile), "#PinModel");
            }

            // Get the real top speed value
            FillFirstPlaceholder(Parameters.TopSpeed / PhysicsConstants.MPH2OGU_SPEED, "#TopSpeed");

            // Fill the car name
            FillFirstPlaceholder(Parameters.Name, "#Name");

            // Get the texture file to use
            FillFirstPlaceholder(Parameters.BodyParameters.Materials.First().Value.TextureFile, "#TextureFile");
        }

        /// <summary>
        /// Auto-fills placeholders with the given object properties.
        /// </summary>
        /// <param name="parameters"></param>
        private void FillInfoFromObject(object parameters)
        {
            Type type = parameters.GetType();
            PropertyInfo[] properties = type.GetProperties();
            string prefix = type.Name + ".";

            foreach(PropertyInfo property in properties)
            {
                if (!SerializedTypes.Contains(property.PropertyType))
                    continue;

                object value = property.GetValue(parameters);

                //if(typeof(IEnumerable).IsAssignableFrom(property.PropertyType))
                //{
                //    IEnumerator enumerator = ((IEnumerable)value).GetEnumerator();

                //    while (enumerator.MoveNext())
                //    {
                //        if (!SerializedTypes.Contains(enumerator.Current.GetType()))
                //            continue;

                //        ReplaceFirst(enumerator.Current, prefix + property.Name);
                //    }
                //}
                //else
                //{
                //    if (!SerializedTypes.Contains(property.PropertyType))
                //        continue;

                //    ReplaceFirst(value, prefix + property.Name);
                //}

                FillFirstPlaceholder(value, prefix + property.Name);
            }
        }

        /// <summary>
        /// Fills the first placeholder occurence with the given object.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="placeHolder"></param>
        private void FillFirstPlaceholder(object value, string placeHolder)
        {
            Type objType = value?.GetType();
            string strValue;

            if (value == null)
                strValue = string.Empty;
            else if (SpecificTypeHandlers.ContainsKey(objType))
                strValue = SpecificTypeHandlers[objType](value);
            else
                strValue = value.ToString();

            Content = Content.ReplaceFirst("{" + placeHolder + "}", strValue);
        }

        private static string MatrixHandler(object value)
        {
            Matrix matrix = (Matrix)value;
            return matrix.Right.ToString() + "\r\n\t\t\t" + matrix.Up.ToString() + "\r\n\t\t\t" + matrix.Look.ToString();
        }

        private static string VectorHandler(object value)
        {
            Vector3 vect = (Vector3)value;
            return new Vector3(vect.X, -vect.Y, vect.Z).ToString();
        }

        private static string EnumHandler(object value)
        {
            return ((int)value).ToString();
        }
    }
}
