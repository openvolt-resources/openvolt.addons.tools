﻿using OpenVolt.Addons.Tools.GameModes.CarEditor.Classes;
using OpenVolt.Addons.Tools.GameModes.CarEditor.Enums;
using OpenVolt.Addons.Tools.GameModes.CarEditor.IO;
using OpenVolt.Addons.Tools.GameModes.CarEditor.ViewModels.Tabs;
using OpenVolt.Interfaces.Car;
using OpenVolt.Interfaces.GameMode;
using OpenVolt.Interfaces.Input;
using OpenVolt.Interfaces.Physics.Body;
using OpenVolt.Interfaces.Physics.Particle;
using OpenVolt.Interfaces.UI;
using System;
using System.Collections.Generic;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor
{
    /// <summary>
    /// Car editor data.
    /// </summary>
    class CarEditorData : IGameModeDataMultiplePlayers, IGameModeData
    {
        /// <summary>
        /// Gets or sets the camera target particle.
        /// </summary>
        public IParticleEntity TargetParticle { get; set; }
        
        /// <summary>
        /// Gets or sets the current screen displayed to player.
        /// </summary>
        public IUIElement CurrentScreen { get; set; }

        /// <summary>
        /// Gets or sets the current loaded car.
        /// </summary>
        public ICarEntity CurrentCar { get; set; }

        /// <summary>
        /// Gets or sets the player controller.
        /// </summary>
        public ITrackedPlayerCarController TrackedPlayerCarController { get; set; }

        /// <summary>
        /// Gets or sets the AI controller.
        /// </summary>
        public IAICarController AICarController { get; set; }

        /// <summary>
        /// Gets or sets the player list. Not used except by the AI controller to find the closest player.
        /// </summary>
        public List<ITrackedCarController> Players { get; set; }

        /// <summary>
        /// Gets or sets the body move handlers.
        /// </summary>
        public Action<IParticleEntity> BodyOnParticleMove { get; set; }

        /// <summary>
        /// Gets or sets the car move handlers called before the body move.
        /// </summary>
        public Action<IBodyEntity> CarOnBeforeBodyMoved { get; set; }

        /// <summary>
        /// Gets or sets the car move handlers called after the body move.
        /// </summary>s
        public Action<IBodyEntity> CarOnAfterBodyMoved { get; set; }

        /// <summary>
        /// Gets or sets the current angle around Y axis.
        /// </summary>
        public float LookAngleX { get; set; }

        /// <summary>
        /// Gets or sets the current angle around X axis.
        /// </summary>
        public float LookAngleY { get; set; }

        /// <summary>
        /// Gets or sets the car parameter tab view model.
        /// </summary>
        public CarParameterTabViewModel CarParameterTabViewModel { get; set; }

        /// <summary>
        /// Gets or sets the body centre parameter tab view model.
        /// </summary>
        public ParticleParameterTabViewModel ParticleParameterTabViewModel { get; set; }

        /// <summary>
        /// Gets or sets the body parameter tab view model.
        /// </summary>
        public BodyParameterTabViewModel BodyParameterTabViewModel { get; set; }

        /// <summary>
        /// Gets or sets the wheel parameter collection tab view model.
        /// </summary>
        public WheelParameterCollectionTabViewModel WheelParameterCollectionTabViewModel { get; set; }

        /// <summary>
        /// Gets or sets the hull editor instance.
        /// </summary>
        public HullPlaneEditor HullPlaneEditor { get; set; }

        /// <summary>
        /// Gets or sets the current opened tab name.
        /// </summary>
        public TabName CurrentTabName { get; set; }

        /// <summary>
        /// Gets or sets the car collision handlers called before the body collision process.
        /// </summary>s
        public Action<IBodyEntity> CarOnBeforeCollisionProcessing { get; set; }

        /// <summary>
        /// Gets or sets the car collision handlers called after the body collision process.
        /// </summary>s
        public Action<IBodyEntity> CarOnAfterCollisionProcessing { get; set; }

        /// <summary>
        /// Gets or sets the body collision handlers called after the particle collision process.
        /// </summary>s
        public Action<IParticleEntity> BodyOnAfterCollisionProcessing { get; set; }

        /// <summary>
        /// Gets or sets the axle parameter collection tab view model.
        /// </summary>
        public AxleParameterCollectionTabViewModel AxleParameterCollectionTabViewModel { get; set; }

        /// <summary>
        /// Gets or sets the spring parameter collection tab view model.
        /// </summary>
        public SpringParameterCollectionTabViewModel SpringParameterCollectionTabViewModel { get; set; }

        /// <summary>
        /// Gets or sets the model editor instance.
        /// </summary>
        public ModelEditor ModelEditor { get; set; }

        /// <summary>
        /// Gets or sets the Re-Volt parameter export helper instance.
        /// </summary>
        public RvCarParameterExportHelper RvCarParameterExportHelper { get; set; }
        public Action<IBodyEntity> CarOnUpdateTick { get; internal set; }

        public CarEditorData()
        {
            Players = new List<ITrackedCarController>();
        }
    }
}
