﻿using OpenVolt.SDK.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor.Classes
{
    /// <summary>
    /// Stores entity parameter info.
    /// </summary>
    class EntityParameterInfo
    {
        /// <summary>
        /// Gets or sets the text to display to the player for this parameter.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the property name.
        /// </summary>
        public string Property { get; set; }
    }
}
