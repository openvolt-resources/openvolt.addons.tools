﻿using OpenVolt.Addons.Tools.GameModes.CarEditor.Enums;
using OpenVolt.Addons.Tools.GameModes.CarEditor.ViewModels.ModelEditor;
using OpenVolt.Interfaces.Car;
using OpenVolt.Interfaces.Engine;
using OpenVolt.Interfaces.IO;
using OpenVolt.Interfaces.Model;
using OpenVolt.Interfaces.Physics.Particle;
using OpenVolt.Interfaces.Scene;
using OpenVolt.Services;
using OpenVolt.Shared;
using OpenVolt.Shared.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor.Classes
{
    /// <summary>
    /// Model editor.
    /// </summary>
    class ModelEditor : IDisposable
    {
        /// <summary>
        /// Gets or sets the view model.
        /// </summary>
        public ModelEditorViewModel ViewModel { get; set; }

        private bool _visible;
        /// <summary>
        /// Gets or sets if the center of mass is visible.
        /// </summary>
        public bool Visible
        {
            get
            {
                return _visible;
            }
            set
            {
                _visible = value;
                CenterOfMassModel.Enabled = _visible;
            }
        }

        /// <summary>
        /// Gets or sets the car to edit.
        /// </summary>
        private ICarEntity Car { get; set; }

        /// <summary>
        /// Gets or sets the car editor controller to use to catch user input.
        /// </summary>
        private CarEditorController Controller { get; set; }

        /// <summary>
        /// Gets or sets the position value setter of the car component to move.
        /// </summary>
        private Action<Vector3> ComponentPositionSetter { get; set; }

        /// <summary>
        /// Gets or sets the model position value setter of the car component to move.
        /// </summary>
        private Action<Vector3> ComponentModelPositionSetter { get; set; }

        /// <summary>
        /// Gets or sets the delegate that trigger the car component parameter process.
        /// </summary>
        private Action TriggerProcessParameters { get; set; }

        /// <summary>
        /// Gets or sets the matrix value getter of the car component to move.
        /// </summary>
        private Func<Matrix> ComponentMatrixGetter { get; set; }

        /// <summary>
        /// Gets or sets the car component model.
        /// </summary>
        private IEngineModelWrapper ComponentModel { get; set; }

        /// <summary>
        /// Gets or sets the camera target particle to use to get camera position and rotation.
        /// </summary>
        private IParticleEntity TargetParticle { get; set; }

        /// <summary>
        /// Gets or sets the center of mass model.
        /// </summary>
        private IEngineModelWrapper CenterOfMassModel { get; set; }

        public ModelEditor(ICarEntity car, CarEditorController controller, IParticleEntity staticParticle)
        {
            TargetParticle = staticParticle;
            Controller = controller;
            Car = car;
            ViewModel = new ModelEditorViewModel();

            CreateCenterOfMassModel();
            CreateComponentViewModels();
        }

        /// <summary>
        /// Creates the center of mass model.
        /// </summary>
        private void CreateCenterOfMassModel()
        {
            IModelGroup model = Service.GetInstance<FileLoaderService>().Get<IModelLoadHelper>().Load(Paths.MODELS + "car_editor/sphere.m");

            CenterOfMassModel = Service.GetInstance<EngineFeatureService>().Get<IEngineSceneManager>().CreateSceneObject<IEngineModelWrapper>();
            CenterOfMassModel.SetModel(model, new Dictionary<int, MaterialInfo>
            {
                { -1, new MaterialInfo
                {
                    Diffuse = Color.White
                } }
            });
            CenterOfMassModel.Enabled = false;
            CenterOfMassModel.EnableShadows = false;
            CenterOfMassModel.Layer = SceneLayer.Objects;
            CenterOfMassModel.Scale = new Vector3(0.2f, 0.2f, 0.2f);
        }

        /// <summary>
        /// Creates the car component view models.
        /// </summary>
        private void CreateComponentViewModels()
        {
            // Body view model
            ViewModel.Components.Add(new ComponentViewModel
            {
                LoadModel = null,
                MoveModel = (el, data) => SelectComponentToMove("Body"),
                Name = "Center of mass"
            });

            // Wheels and attached components
            for(int i = 0; i < 4; i++)
            {
                int id = i;
                // Wheel view model
                ViewModel.Components.Add(new ComponentViewModel
                {
                    LoadModel = null,
                    MoveModel = (el, data) => SelectComponentToMove($"Wheel {id}"),
                    Name = $"Wheel {i}"
                });
            }

            for (int i = 0; i < 4; i++)
            {
                int id = i;
                // Spring view model
                ViewModel.Components.Add(new ComponentViewModel
                {
                    LoadModel = null,
                    MoveModel = (el, data) => SelectComponentToMove($"Spring {id}"),
                    Name = $"Spring {i}"
                });
            }

            for (int i = 0; i < 4; i++)
            {
                int id = i;
                // Axle view model
                ViewModel.Components.Add(new ComponentViewModel
                {
                    LoadModel = null,
                    MoveModel = (el, data) => SelectComponentToMove($"Axle {id}"),
                    Name = $"Axle {i}"
                });
            }
        }

        /// <summary>
        /// Sets the car component to move.
        /// </summary>
        /// <param name="info"></param>
        private void SelectComponentToMove(string info)
        {
            // Reset delegates
            ComponentPositionSetter = null;
            ComponentMatrixGetter = null;
            ComponentModelPositionSetter = null;
            TriggerProcessParameters = null;

            // Restore previous component default materials
            ComponentModel?.RestoreSharedMaterials();
            ComponentModel = null;

            CenterOfMassModel.Enabled = false;

            // Make all components partially transparent
            SetOpacity(0.5f);

            // Set delegates depending on car component
            if (info == "Body")
            {
                ComponentPositionSetter = (v) => Car.Body.Parameters.CenterOfMassOffset -= v;
                ComponentModelPositionSetter = (v) =>
                {
                    CenterOfMassModel.Position += v;
                    Car.Body.Centre.Data.Position += v;
                };
                ComponentMatrixGetter = () => Car.Body.Centre.Data.WorldMatrix;
                TriggerProcessParameters = Car.Body.RefreshCenterOfMass;
                ComponentModel = CenterOfMassModel;
                CenterOfMassModel.Enabled = true;
                CenterOfMassModel.Position = Car.Body.Centre.Data.Position;
            }
            else if(info.StartsWith("Wheel"))
            {
                int id = int.Parse(info.Split(' ')[1]);

                ComponentPositionSetter = (v) => Car.Wheels[id].Parameters.FixingPointOffset += v;
                ComponentModelPositionSetter = (v) => Car.Wheels[id].ModelObject.Position += v;
                ComponentMatrixGetter = () => Car.Wheels[id].Data.WorldMatrix;
                TriggerProcessParameters = Car.Wheels[id].TriggerProcessParameters;
                ComponentModel = Car.Wheels[id].ModelObject;
            }
            else if (info.StartsWith("Axle"))
            {
                int id = int.Parse(info.Split(' ')[1]);

                ComponentPositionSetter = (v) => Car.Axles[id].Parameters.Offset += v;
                ComponentModelPositionSetter = (v) => Car.Axles[id].ModelObject.Position += v;
                ComponentMatrixGetter = () => Car.Axles[id].Data.WorldMatrix;
                TriggerProcessParameters = Car.Axles[id].TriggerProcessParameters;
                ComponentModel = Car.Axles[id].ModelObject;
            }
            else if (info.StartsWith("Spring"))
            {
                int id = int.Parse(info.Split(' ')[1]);

                ComponentPositionSetter = (v) => Car.Springs[id].Parameters.Offset += v;
                ComponentModelPositionSetter = (v) => Car.Springs[id].ModelObject.Position += v;
                ComponentMatrixGetter = () => Car.Springs[id].Data.WorldMatrix;
                TriggerProcessParameters = Car.Springs[id].TriggerProcessParameters;
                ComponentModel = Car.Springs[id].ModelObject;
            }

            // No transparency for the selected component
            if (ComponentModel != null)
                ComponentModel.RestoreSharedMaterials();
        }

        /// <summary>
        /// Sets the opacity of all car component models.
        /// </summary>
        /// <param name="opacity"></param>
        public void SetOpacity(float opacity)
        {
            // If no opacity, just restore default materials
            if(opacity == 1.0f)
            {
                Car.Body.ModelObject.RestoreSharedMaterials();

                for (int i = 0; i < 4; i++)
                {
                    Car.Wheels[i].ModelObject.RestoreSharedMaterials();
                    Car.Springs[i].ModelObject.RestoreSharedMaterials();
                    Car.Axles[i].ModelObject.RestoreSharedMaterials();
                }

                return;
            }

            Car.Body.ModelObject.Opacity = opacity;

            for(int i = 0; i < 4; i++)
            {
                Car.Wheels[i].ModelObject.Opacity = opacity;
                Car.Springs[i].ModelObject.Opacity = opacity;
                Car.Axles[i].ModelObject.Opacity = opacity;
            }
        }

        /// <summary>
        /// Moves the selected car component.
        /// </summary>
        public void MoveModel()
        {
            // Skip if no selected component
            if (ComponentPositionSetter == null)
                return;

            if (Controller.InputManager.IsActionActive(EditorAction.Move))
            {
                // Calculate camera alignment with component
                Vector2 slideDelta = new Vector2(Controller.InputManager.GetActionValue(EditorAction.SlideX), Controller.InputManager.GetActionValue(EditorAction.SlideY));
                Vector3 worldDelta = slideDelta.Project2DOn3D(TargetParticle.Data.WorldMatrix, Car.Body.Centre.Data.WorldMatrix);
                Vector3 localDelta = worldDelta * Car.Body.Centre.Data.WorldMatrix.Invert();

                ComponentPositionSetter(localDelta);
                ComponentModelPositionSetter(worldDelta);

                TriggerProcessParameters();
            }
        }

        public void Dispose()
        {
            CenterOfMassModel.Destroy();
        }
    }
}
