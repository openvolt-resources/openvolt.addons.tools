﻿using OpenVolt.Addons.Tools.GameModes.CarEditor.Enums;
using OpenVolt.Addons.Tools.GameModes.CarEditor.ViewModels.HullPlaneEditor;
using OpenVolt.Interfaces.Camera;
using OpenVolt.Interfaces.Car;
using OpenVolt.Interfaces.Engine;
using OpenVolt.Interfaces.Model;
using OpenVolt.Interfaces.Physics.Colliders;
using OpenVolt.Interfaces.Physics.Particle;
using OpenVolt.Interfaces.Scene;
using OpenVolt.SDK;
using OpenVolt.Services;
using OpenVolt.Shared;
using OpenVolt.Shared.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor.Classes
{
    /// <summary>
    /// Hull editor.
    /// </summary>
    class HullPlaneEditor : IDisposable
    {
        /// <summary>
        /// Gets or sets the view model.
        /// </summary>
        public HullPlaneEditorViewModel ViewModel { get; private set; }

        private bool _visible;
        /// <summary>
        /// Gets or sets if the planes are visible.
        /// </summary>
        public bool Visible
        {
            get
            {
                return _visible;
            }
            set
            {
                _visible = value;

                foreach (IEngineModelWrapper model in ConvexModels.SelectMany(x => x))
                    model.Enabled = _visible;
            }
        }

        /// <summary>
        /// Gets or sets the collider to edit.
        /// </summary>
        private IHullCollider Collider { get; set; }

        /// <summary>
        /// Gets or sets the car to edit.
        /// </summary>
        private ICarEntity Car { get; set; }

        /// <summary>
        /// Gets or sets the plane model list drawn in the scene.
        /// </summary>
        private List<List<IEngineModelWrapper>> ConvexModels { get; set; }

        /// <summary>
        /// Gets or sets the scene manager instance.
        /// </summary>
        private IEngineSceneManager SceneManager { get; set; }

        /// <summary>
        /// Gets or sets the selected plane.
        /// </summary>
        private HullPlane SelectedPlane { get; set; }

        /// <summary>
        /// Gets or sets the model of the selected plane.
        /// </summary>
        private IEngineModelWrapper SelectedPlaneModel { get; set; }

        /// <summary>
        /// Gets or sets the car editor controller to use to catch user input.
        /// </summary>
        private CarEditorController Controller { get; set; }

        /// <summary>
        /// Gets or sets the camera target particle to use to get camera position and rotation.
        /// </summary>
        private IParticleEntity TargetParticle { get; set; }

        public HullPlaneEditor(ICarEntity car, CarEditorController controller, IParticleEntity targetParticle)
        {
            TargetParticle = targetParticle;
            Controller = controller;
            Car = car;
            Collider = car.Body.Data.Colliders.FirstOrDefault(x => x.Type == ColliderType.Hull);
            ConvexModels = new List<List<IEngineModelWrapper>>();
            SceneManager = Service.GetInstance<EngineFeatureService>().Get<IEngineSceneManager>();
            ViewModel = new HullPlaneEditorViewModel();

            // Discover hull convexes
            for(int i = 0; i < Collider.Convexes.Count; i++)
            {
                List<HullPlane> convex = Collider.Convexes[i];
                List<IEngineModelWrapper> planeModels = new List<IEngineModelWrapper>();
                // Create the convex view model
                ConvexViewModel convexVm = new ConvexViewModel();

                ConvexModels.Add(planeModels);
                ViewModel.Convexes.Add(convexVm);

                convexVm.Text = $"Convex {i + 1}";

                // Discover convex planes
                for(int j = 0; j < convex.Count; j++)
                {
                    HullPlane plane = convex[j];
                    IEngineModelWrapper planeModel = SceneManager.CreateSceneObject<IEngineModelWrapper>();
                    // Create plane view model
                    PlaneViewModel planeVm = new PlaneViewModel();
                    int convexId = i;
                    int planeId = j;

                    convexVm.Planes.Add(planeVm);
                    planeVm.Text = $"Plane {j + 1}";
                    planeVm.ShowPlane = (el, data) => SelectPlane(convexId, planeId);

                    // Create plane model
                    planeModel.Parent = car.Body.Centre.ModelObject;
                    planeModel.LocalPosition = -plane.RelativePlane.Normal * plane.RelativePlane.Distance;
                    planeModel.Layer = SceneLayer.Objects;
                    planeModel.Enabled = false;
                    planeModel.SetModel(CreatePlaneModel(100), new Dictionary<int, MaterialInfo>
                    {
                        { 0, new MaterialInfo
                        {
                            Diffuse = new Color(0, 1, 0, 0.1f)
                        } }
                    });
                    planeModel.LocalRotation = Matrix.FromUp(plane.RelativePlane.Normal);
                    planeModel.EnableShadows = false;
                    planeModel.RecalculateBounds();
                    planeModels.Add(planeModel);
                }
            }
        }

        /// <summary>
        /// Selects the specified plane of the specified convex.
        /// </summary>
        /// <param name="convexId"></param>
        /// <param name="planeId"></param>
        public void SelectPlane(int convexId, int planeId)
        {
            // Restore all planes default color
            foreach(IEngineModelWrapper model in ConvexModels.SelectMany(x => x))
                model.RestoreSharedMaterials();

            SelectedPlane = null;
            SelectedPlaneModel = null;

            if (convexId < 0 || convexId >= ConvexModels.Count)
                return;

            if (planeId < 0 || planeId >= ConvexModels[convexId].Count)
                return;

            // Find plane
            SelectedPlane = Collider.Convexes[convexId][planeId];
            SelectedPlaneModel = ConvexModels[convexId][planeId];

            // Make the plane color red
            SelectedPlaneModel.UpdateCustomMaterials(new Dictionary<int, MaterialInfo>
            {
                { 0, new MaterialInfo
                {
                    Diffuse = new Color(1, 0, 0, 0.5f)
                } }
            });
        }

        /// <summary>
        /// Moves the selected plane.
        /// </summary>
        public void MovePlane()
        {
            // Skip if no selected plane
            if (SelectedPlane == null)
                return;

            if (Controller.InputManager.IsActionActive(EditorAction.Move))
            {
                // Calculate camera alignment with plane normal
                float scaleX = Vector3.Dot(TargetParticle.Data.WorldMatrix.Right, SelectedPlane.RelativePlane.Normal * Car.Body.Centre.Data.WorldMatrix);
                float scaleY = Vector3.Dot(TargetParticle.Data.WorldMatrix.Up, SelectedPlane.RelativePlane.Normal * Car.Body.Centre.Data.WorldMatrix);

                // Apply slid delta
                SelectedPlane.RelativePlane.Distance -= Controller.InputManager.GetActionValue(EditorAction.SlideX) * scaleX;
                SelectedPlane.RelativePlane.Distance -= Controller.InputManager.GetActionValue(EditorAction.SlideY) * scaleY;

                // Update the plane model position
                SelectedPlaneModel.LocalPosition = -SelectedPlane.RelativePlane.Normal * SelectedPlane.RelativePlane.Distance;
            }
        }

        /// <summary>
        /// Create a 3D plane of the given size.
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        private IModelGroup CreatePlaneModel(float size)
        {
            IModelGroup modelGroup = ModelProxy.CreateModelGroup();
            IModelData modelData = ModelProxy.CreateModelData();

            // Vertices on X and Z axes
            modelData.Vertices = new List<Vector3>()
            {
                new Vector3(-size, 0, -size),
                new Vector3(-size, 0, size),
                new Vector3(size, 0, size),
                new Vector3(size, 0, -size)
            };

            // Double-sided
            modelData.SubMeshTriangles = new Dictionary<int, List<int>>()
            {
                {
                    0, new List<int>()
                    {
                        0, 1, 2, 2, 3, 0,
                        0, 2, 1, 2, 0, 3
                    }
                }
            };

            // Default normals
            modelData.Normals = new List<Vector3>()
            {
                Vector3.Up,
                Vector3.Up,
                Vector3.Up,
                Vector3.Up
            };

            // Default texture mapping
            modelData.UVs = new List<Vector2>()
            {
                Vector2.One,
                Vector2.One,
                Vector2.One,
                Vector2.One
            };

            // Create a material for the first sub meshj
            modelData.GetMaterialInfo(0);

            // Add the mesh to the first LOD
            modelGroup.AddLodModel(0, modelData);

            return modelGroup;
        }

        public void Dispose()
        {
            foreach(IEngineModelWrapper model in ConvexModels.SelectMany(x => x))
                model.Destroy();
        }
    }
}
