﻿using OpenVolt.Addons.Tools.GameModes.CarEditor.Enums;
using OpenVolt.SDK.Input;
using OpenVolt.Shared.Enums;
using static OpenVolt.SDK.Input.InputManager<OpenVolt.Addons.Tools.GameModes.CarEditor.Enums.EditorAction>;

namespace OpenVolt.Addons.Tools.GameModes.CarEditor
{
    class CarEditorController : ControllerBase<EditorAction>
    {
        public CarEditorController()
        {
            InputManager.Bind(EditorAction.Rotate, InputManager.BinaryInput(InputMouseButton.RightClick));
            InputManager.Bind(EditorAction.Move, InputManager.BinaryInput(InputMouseButton.LeftClick));
            InputManager.Bind(EditorAction.Zoom, new InputValueProvider(() => EngineInput.GetAxis(InputAxis.MouseWheel)));
            InputManager.Bind(EditorAction.SlideX, new InputValueProvider(() => EngineInput.GetAxis(InputAxis.MouseX)));
            InputManager.Bind(EditorAction.SlideY, new InputValueProvider(() => EngineInput.GetAxis(InputAxis.MouseY)));
        }
    }
}
